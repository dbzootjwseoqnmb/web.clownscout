@extends('dashboard')
@section('title', 'Rabia Tools')
@section('cardtitle', 'Local Accounts')
@section('cardsubtitle', 'The accounts which are to be distributed among the customers - Total Customers:  '.$localcount)
@section('body')

<div class="row">
    <div class="col-md-12">
        <div class="card" >
            <div class="card-header card-header-primary" >
                <h4 class="card-title ">Add Local Accounts</h4>
                <p class="card-category">Select parent JS account from drop down and then add local account details</p>
            </div>

            <div class="card-body">

                @if(!isset($isAdmin))
                    <form action="{{route('admin.scout.add.account.local')}}" method="POST" >
                        @else
                            <form action="{{route('admin.add.admins')}}" method="POST" >
                                @endif
                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">JS Account DropDown</label>
                                    <select class="form-control" name="js_id">
                                        <option value="0">None</option>
                                        @foreach($jsaccounts as $js)
                                            <option value="{{$js->id}}">{{$js->email}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Keepa Account DropDown</label>
                                    <select class="form-control" name="keepa_id">
                                        <option value="0">None</option>
                                        @foreach($keepa as $k)
                                            <option value="{{$k->id}}">{{$k->email}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Udemy Account DropDown</label>
                                    <select class="form-control" name="udemy_id">
                                        <option value="0">None</option>
                                        @foreach($udemy as $k)
                                            <option value="{{$k->id}}">{{$k->email}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Helium Account DropDown</label>
                                    <select class="form-control" name="helium_id">
                                        <option value="0">None</option>
                                        @foreach($helium as $helium)
                                            <option value="{{$helium->id}}">{{$helium->email}}</option>
                                        @endforeach
                                    </select>
                                @endif
                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Local User Name</label>
                                @else
                                    <label class="bmd-label-floating">Admin User Name</label>
                                @endif
                                <input type="text" name="name" class="form-control">

                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Local Account Email</label>
                                @else
                                    <label class="bmd-label-floating">Admin Account Email</label>
                                @endif

                                <input type="email" name="email" class="form-control">

                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Local Account Password</label>
                                @else
                                    <label class="bmd-label-floating">Admin Account Password</label>
                                @endif


                                <input type="password" name="password" class="form-control">

                                @if(!isset($isAdmin))
                                    <label class="bmd-label-floating">Subscriptions</label><br>
                                    <input type="checkbox" name="subscriptions[]" value="1" checked> JS Web
                                    <input type="checkbox" name="subscriptions[]" value="2" checked> JS Extension
                                @endif
                                {{csrf_field()}}
                                <input type="submit" class="btn btn-primary" value="Add Local Account">
                            </form>

            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <form action="" method="GET">
                            <input type="text" name="search" class="form-control" placeholder="Search by name">
                            <input type="submit" class="btn btn-primary" value="Name search">
                        </form>
                    </div>
                    <div class="col-md-6">
                        <form action="" method="GET">
                            <input type="text" name="email" class="form-control" placeholder="Search by email">
                            <input type="submit" class="btn btn-primary" value="Email search">
                        </form>
                    </div>
                </div>
                <br>

                {{$localaccount->withQueryString()->links()}}
            </div>
        </div>
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">@yield('cardtitle')</h4>
                <p class="card-category">@yield('cardsubtitle')</p>
            </div>
            <div class="card-body">
                @php ($i = 1)
                <div class="col-md-12">
                    <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Name</th>
                        @if(!isset($isAdmin))
                        <th>Local Email</th>
                        @else
                        <th>Admin Email</th>
                        @endif

                        @if(!isset($isAdmin))
                        <th>JS Account</th>
                        @endif
                        @if(!isset($isAdmin))
                        <th>Helium Account</th>
                        @endif

                        @if(!isset($isAdmin))
                        <th>Subscriptions</th>
                        @endif

                        <th>Action</th>
                    </thead>

                        @foreach($localaccount as $js)
                        <tr>
                            <td>{{$js->id}}</td>
                            <td>{{$js->name}}</td>
                            <td>{{$js->email}}</td>
                            @if(!isset($isAdmin))
                                @if($js->js_id != 0)
                                    <td>{{$js->js->email}}</td>
                                @else
                                    <td></td>
                                @endif
                            @if($js->helium_id != 0)
                                <td>{{$js->helium->email}}</td>
                            @else
                                <td></td>
                            @endif
                            <td>{!!subscriptions($js->roles->toArray(), $js->helium_id, $js->keepa_id)!!}</td>
                            @endif
                            <td class="td-actions text-right">

                                <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                    <a class="tokendelete" href="{{route('admin.delete.local')}}/{{$js->id}}">
                                        <i class="material-icons">close</i>
                                    </a>
                                </button>
                        </td>
                        </tr>
                        @endforeach

                    </table>
                </div>

            </div>


        </div>
        <div class="card">
            <div class="card-body">
                {{$localaccount->withQueryString()->links()}}
            </div>
        </div>


    </div>




</div>
</div>
<script>

$('.tokendelete').on('click', function(e){
    var c = confirm("Do you really want to delete this local account?");
    if(!c) {
        e.preventDefault();
    }
    // alert('ok');
});

</script>

@endsection
