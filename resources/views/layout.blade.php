<!doctype html>
<html lang="en">

<head>
  <title>@yield('title')</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <link href="{{url('/')}}/assets/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
  <script src="{{url('/')}}/assets/js/jquery.min.js"></script>
  <script src="{{url('/')}}/assets/js/core/popper.min.js"></script>
  <script src="{{url('/')}}/assets/js/core/bootstrap-material-design.min.js"></script>
</head>
<body>
    <div class="wrapper ">
        <div class="sidebar" data-color="green" data-background-color="white">
            <div class="logo">
                @if(request()->getHost() == "members.amzpremiumtools.com")
                    <a href="https://amzpremiumtools.com" class="simple-text logo-normal">
                        <img src="https://members.amzpremiumtools.com/asset/images/logo-amzpremium.png" style="width: 250px" alt="logo">
                    </a>
                @else
                    <a href="https://amzvatools.com" class="simple-text logo-normal">
                        <img src="https://members.amzpremiumtools.com/asset/images/logo-amzpremium.png" style="width: 250px" alt="logo">
                    </a>

                @endif
            </div>


            <!-- menu bar -->
            <div class="sidebar-wrapper">
                <ul class="nav">
                @include('menu')
                <!-- your sidebar here -->
                </ul>
            </div>


        </div>


        <div class="main-panel">
            <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                <div class="container-fluid">
                    <div class="navbar-wrapper">
                        <a class="navbar-brand" href="javascript:;">@yield('subtitle')</a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                        <span class="navbar-toggler-icon icon-bar"></span>
                    </button>
                </div>
            </nav>
            <!--end of nav bar -->


            <div class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
            <!--content end -->
        </div>
    </div>
</body>
</html>




