<li class="nav-item {{ (route('admin.dashboard') === url()->full()) ? 'active' : ''}}">
    <a class="nav-link" href="{{route('admin.dashboard')}}">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
    </a>
</li>
<li class="nav-item {{ (route('admin.scout.account') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.scout.account')}}"><i class="material-icons">dashboard</i><p>Jungle Scout Account</p></a></li>
<li class="nav-item {{ (route('admin.helium.account') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.helium.account')}}"><i class="material-icons">dashboard</i><p>Helium 10 Account</p></a></li>
<li class="nav-item {{ (route('admin.keepa.account') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.keepa.account')}}"><i class="material-icons">dashboard</i><p>Keepa Account</p></a></li>
<li class="nav-item {{ (route('admin.udemy.account') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.udemy.account')}}"><i class="material-icons">dashboard</i><p>Udemy Account</p></a></li>
<li class="nav-item {{ (route('admin.scout.account.local') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.scout.account.local')}}"><i class="material-icons">dashboard</i><p>Add Local Accounts</p></a></li>
<li class="nav-item {{ (route('admin.switch') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('admin.switch')}}"><i class="material-icons">dashboard</i><p>Switch Accounts</p></a></li>
<li class="nav-item {{ (route('logout') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('logout')}}"><i class="material-icons">dashboard</i><p>Logout</p></a></li>
