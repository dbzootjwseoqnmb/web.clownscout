@extends('dashboard')
@section('title', 'JS Accounts')
@section('cardtitle', 'Local Accounts')
@section('cardsubtitle', 'The accounts which are to be distributed among the customers ')
@section('body')

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">@yield('cardtitle')</h4>
                <p class="card-category">@yield('cardsubtitle')</p>
            </div>
            <div class="body-card">
            </div>


        </div>



    </div>



    <div class="col-md-5"> 
    <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">Add Local Accounts</h4>
            <p class="card-category">Select parent JS account from drop down and then add local account details</p>
        </div>

        <div class="card-body">
            <form action="{{route('admin.scout.add.account.local')}}" method="POST" >
            <label class="bmd-label-floating">JS Account DropDown</label>
                
                <label class="bmd-label-floating">Local User Name</label>0
                <input type="text" name="name" class="form-control">
                <label class="bmd-label-floating">Local Account Email</label>
                <input type="email" name="email" class="form-control">
                <label class="bmd-label-floating">Local Account Password</label>
                <input type="password" name="password" class="form-control">
                {{csrf_field()}}
                <input type="submit" class="btn btn-primary" value="Add Local Account">
            </form>

        </div>

    </div>
    </div>
</div>
</div>
<script>

$('.tokendelete').on('click', function(e){
    var c = confirm("Do you really want to delete this local account?");
    if(!c) {
        e.preventDefault();
    }
    // alert('ok');
});

</script>

@endsection
