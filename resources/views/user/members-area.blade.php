@extends('user.dashboard')

@if(request()->getHost() == "members.amzpremiumtools.com")
    @section('cardtitle', 'RabiTools Extension')
    @section('cardsubtitle', 'Extension is required to access JS Web and JS Extension')
    @section('title', 'Rabia Tools')


@else
    @section('cardtitle', 'AMZ VA-Tool Extension')
    @section('cardsubtitle', 'Extension is required to access JS Web and JS Extension')
    @section('title', 'AMZ VA-Tool')

@endif
@section('body')

<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header card-header-primary">
                <div class="card-title font-weight-bold">
                    <h4 class="card-title font-weight-bold ">Tutorial</h4>
                </div>
            </div>
            <div class="body-card">
                <center>
                    <br>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/CmDixlQpiJ0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </center>
            </div>
        </div>

        <div class="card">
            @if(request()->getHost() == "members.amzpremiumtools.com")
                      <div class="card-header card-header-primary">
            @else
                      <div class="card-header card-header-success">
            @endif
                @yield('cardtitle')</h4>
                <p class="card-category">@yield('cardsubtitle')</p>
            </div>
            <div class="body-card">
                <div class="d-flex justify-content-center">
                    @if(request()->getHost() == "members.amzpremiumtools.com")
                        <a href="https://members.amzpremiumtools.com/RabiaExt.zip" class="btn btn-warning">Download Extension</a>
                    @else
                        <a href="https://amzvatools.com/amzva-ext.zip" class="btn btn-warning">Download Extension</a>
                    @endif
                </div>
            </div>


        </div>
    </div>


    @foreach($subscriptions as $sub)
        @if($sub['type'] == 1)
            <div class="col-md-6">
                <div class="card">
                    @if(request()->getHost() == "members.amzpremiumtools.com")
                        <div class="card-header card-header-primary">
                    @else
                        <div class="card-header card-header-success">
                    @endif
                        <h4 class="card-title font-weight-bold ">Jungle Scout Website</h4>
                        <p class="card-category">To access Jungle Scout website, please click on this link.</p>
                    </div>

                    <div class="card-body">
                        <a href="{{route('user.js-web')}}" target="_blank" class="btn btn-warning">JS Web</a>
                    </div>

{{--                    <div class="card-body">--}}
{{--                        <p>--}}
{{--                            How to use JS web and Extension from portal <br>--}}
{{--                            Tutorial: <a href="https://www.loom.com/share/f3fb12d0605944ac88e3803df26437f7" target="_blank">Video Link</a>--}}
{{--                        </p>--}}
{{--                        <p>--}}
{{--                            If you are facing any issue in accessing JS web and Extension <br>--}}
{{--                            Tutorial: <a href="https://www.loom.com/share/4b7ddeb44a904453a853fd7f156f4917" target="_blank">Video Link</a>--}}
{{--                        </p>--}}

{{--                    </div>--}}

                </div>
            </div>
        @endif

        @if($sub['type'] == 2)
            <div class="col-md-6">
                <div class="card">
                    @if(request()->getHost() == "members.amzpremiumtools.com")
                        <div class="card-header card-header-primary">
                    @else
                        <div class="card-header card-header-success">
                    @endif
                        <h4 class="card-title font-weight-bold ">Jungle Scout Extension</h4>
                        <p class="card-category">Download Jungle Scount Extension <small>(you need to be logged into this portal & should have installed our extension)</small></p>
                    </div>

                    <div class="card-body">
                        @if(request()->getHost() == "members.amzpremiumtools.com")
                        <a href="https://members.amzpremiumtools.com/rabia-js-extension.zip" class="btn btn-warning">JS Extension</a>
                        @else
                            <a href="https://amzvatools.com/amzva-js-ext-2022.zip" class="btn btn-warning">JS Extension</a>
                        @endif
                    </div>

                </div>
            </div>
        @endif
    @endforeach

    @if($helium_user)
        <div class="col-md-6">
            <div class="card">
                @if(request()->getHost() == "members.amzpremiumtools.com")
                    <div class="card-header card-header-primary">
                @else
                    <div class="card-header card-header-success">
                @endif
                    <h4 class="card-title font-weight-bold ">Helium 10 Web + Extension</h4>
                    <p class="card-category">To access Helium website, please click on this link. To access extension, download it from official source.</p>
                </div>

                <div class="card-body">
                    <a href="{{route('user.helium-web')}}" target="_blank" class="btn btn-warning">Helium</a>
                </div>

{{--                <div class="card-body">--}}
{{--                    <p>--}}
{{--                        How to Use Helium Via Portal <br>--}}
{{--                        Tutorial: <a href="https://www.loom.com/share/958ce461066c4f7ab80848c7166b513a" target="_blank">Video Link</a>--}}
{{--                    </p>--}}
{{--                </div>--}}
            </div>
        </div>
        @if($helium_ext1)
            <div class="col-md-6">
                <div class="card">
                    @if(request()->getHost() == "members.amzpremiumtools.com")
                              <div class="card-header card-header-primary">
                    @else
                              <div class="card-header card-header-success">
                    @endif
                        <h4 class="card-title font-weight-bold ">Helium Temporary 1</h4>
                        <p class="card-category">To access Helium website, please click on this link. To access extension, download it from official source.</p>
                    </div>

                    <div class="card-body">
                        <a href="{{route('user.helium-web1')}}" target="_blank" class="btn btn-warning">Helium Temporary 1</a>
                    </div>

                </div>
            </div>
        @endif
        @if($helium_ext2)
            <div class="col-md-6">
                <div class="card">
                    @if(request()->getHost() == "members.amzpremiumtools.com")
                              <div class="card-header card-header-primary">
                    @else
                              <div class="card-header card-header-success">
                    @endif
                        <h4 class="card-title ">Helium Temporary 2</h4>
                        <p class="card-category">To access Helium website, please click on this link. To access extension, download it from official source.</p>
                    </div>

                    <div class="card-body">
                        <a href="{{route('user.helium-web2')}}" target="_blank" class="btn btn-warning">Helium Temporary 2</a>
                    </div>

                </div>
            </div>
        @endif
    @endif
    @if($keepa_user)
        <div class="col-md-6">
            <div class="card">
                @if(request()->getHost() == "members.amzpremiumtools.com")
                    <div class="card-header card-header-primary">
                        @else
                            <div class="card-header card-header-success">
                                @endif
                                <h4 class="card-title ">Keepa</h4>
                                <p class="card-category">To access Keepa website, please click on this link.</p>
                            </div>

                            <div class="card-body">
                                <a href="{{route('user.keepa-tab')}}" target="_blank" class="btn btn-warning">Keepa</a>
                            </div>

                    </div>
            </div>
    @endif

    @if($udemy_user)
        <div class="col-md-6">
            <div class="card">
                @if(request()->getHost() == "members.amzpremiumtools.com")
                    <div class="card-header card-header-primary">
                        @else
                            <div class="card-header card-header-success">
                                @endif
                                <h4 class="card-title ">Udemy</h4>
                                <p class="card-category">To access Udemy website, please click on this link.</p>
                            </div>

                            <div class="card-body">
                                <a href="{{route('user.udemy-tab')}}" target="_blank" class="btn btn-warning">Udemy</a>
                            </div>

                    </div>
            </div>
    @endif
</div>
</div>
<script>

$('.tokendelete').on('click', function(e){
    var c = confirm("Do you really want to delete this local account?");
    if(!c) {
        e.preventDefault();
    }
    // alert('ok');
});

</script>

@endsection
