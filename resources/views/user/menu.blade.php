<li class="nav-item {{ (route('user.dashboard') === url()->full()) ? 'active' : ''}}">
    <a class="nav-link" href="{{route('user.dashboard')}}">
        <i class="material-icons">dashboard</i>
        <p>Dashboard</p>
    </a>
</li>
<li class="nav-item {{ (route('user.logout') === url()->full()) ? 'active' : ''}} "><a class="nav-link" href="{{route('user.logout')}}"><i class="material-icons">dashboard</i><p>Logout</p></a></li>
