<html>
    <head>
    <meta name="referrer" content="never">
    </head>

    <body>
        <p id="status">Checking extension...</p>

        <script>
            var statusElement =  document.getElementById("status");
            var isExtensionInstalled = false;
            window.addEventListener("message", function (event) {
                if (event.source == window &&
                    event.data.sender &&
                    event.data.sender === "rabitools") {
                        isExtensionInstalled = true;
                }
            });
            var timer = setInterval(checkForSession, 2000);
            function checkForSession(){
                if(isExtensionInstalled){
                    statusElement.innerHTML = "Extension is installed, redirecting...";

                    clearInterval(timer);
                    var newTimer = setInterval(redirect, 3000);
                } else {
                    statusElement.innerHTML = "Make sure the extension is installed!";

                }
            }

            function redirect(){
                url = "https://members.junglescout.com/";
                window.location.href= url;
            }

        </script>
    </body>

</html>