@extends('user.dashboard')
@section('title', 'Dashboard')
@section('subtitle', 'Member\'s Profile Area')
@section('content')


@section('body')

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div class="card-header card-header-primary">
                <h4 class="card-title ">@yield('cardtitle')</h4>
                <p class="card-category">@yield('cardsubtitle')</p>
            </div>
            <div class="body-card">
            </div>


        </div>



    </div>


@endsection

