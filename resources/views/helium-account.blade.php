@extends('dashboard')
@if(!isset($type))
    @php
        $type = 'helium';
    @endphp
@endif
@if($type == 'helium')
    @section('title', 'Helium Accounts')
@else
    @section('title', 'JS Accounts')
@endif

@if($type == 'helium')
    @section('cardtitle', 'Helium Accounts')
@else
    @section('cardtitle', 'JS Accounts')
@endif
@if($type == 'helium')
    @section('cardsubtitle', 'The accounts which are provided by Helium ')

@else
    @section('cardsubtitle', 'The accounts which are provided by JungleScout ')
@endif
@section('body')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">@yield('cardtitle')</h4>
                    <p class="card-category">@yield('cardsubtitle')</p>
                </div>
                <div class="card-body">

                    <section class="row">
                        <div class="col-md-12">
                            <table class="table">
                                @php ($i = 1)
                                <thead>
                                <th>#</th>
                                <th>Email</th>
                                <th>Session</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                @foreach($helium as $js)
                                    <tr class="{{ ($js->status) ? '' : 'bg-danger' }}">
                                        <td>{{$i++}}</td>
                                        <td>{{$js->email}}</td>
                                        <td>{{substr($js->session, 0, 70)."..."}}</td>
                                        <td>{{($js->status) ? "active" : "inactive"}}</td>
                                        <td class="td-actions text-right">

                                            <button type="button" rel="tooltip" title="Add Session" class="btn btn-danger btn-link btn-sm">
                                                <a class="" href="{{route('admin.helium.token')}}/{{$js->id}}">
                                                    <i class="material-icons">launch</i>
                                                </a>
                                            </button>



                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                <a class="tokendelete" href="{{route('admin.delete.helium.token')}}/{{$js->id}}">
                                                    <i class="material-icons">close</i>
                                                </a>
                                            </button>


                                        </td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>

                    </section>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Add Helium Account</h4>
                    <p class="card-category">Insert Valid Credentails</p>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.helium.add.account')}}" method="POST" >
                        <label class="bmd-label-floating">Helium Account Email</label>
                        <input type="email" name="email" class="form-control">

                        <label class="bmd-label-floating">Password</label>
                        <input type="password" name="password" class="form-control">
                        {{csrf_field()}}

                        <label class="bmd-label-floating">Session</label>
                        <textarea class="form-control" name="sess"></textarea>
                        <input type="submit" class="btn btn-primary" value="Add Helium Account">
                    </form>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>


    </div>
    @if(isset($updateSession))
        <div class="row">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Update Helium Web Session</h4>
                        <p class="card-category">Login to original JS Web, and copy the cookies and paste here.</p>
                    </div>
                    <div class="card-body">

                        <section class="row">
                            <div class="col-md-12">
                                <form action="{{route('admin.add.helium.token')}}" method="post">
                                    <textarea rows=15 class="form-control" name="session">{{$content}}</textarea>
                                    {{csrf_field()}}
                                    <input type="hidden" name="js_id" value="{{$js_id}}">
                                    <input type="submit" class="btn btn-primary" value="Update Session">

                                </form>
                            </div>
                        </section>
                    </div>
                    @endif
                    <script>

                        $('.tokendelete').on('click', function(e){
                            var c = confirm("If you delete this account, all LOCAL ACCOUNT associated with the account will not be able to access Helium. Are you sure you want to continue?");
                            if(!c) {
                                e.preventDefault();
                            }
                            // alert('ok');
                        });

                    </script>
@endsection
