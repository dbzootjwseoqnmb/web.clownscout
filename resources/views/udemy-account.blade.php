@extends('dashboard')
@if(!isset($type))
    @php
        $type = 'udemy';
    @endphp
@endif
@if($type == 'udemy')
    @section('title', 'Udemy Accounts')
@else
    @section('title', 'JS Accounts')
@endif

@if($type == 'udemy')
    @section('cardtitle', 'Udemy Accounts')
@else
    @section('cardtitle', 'JS Accounts')
@endif
@if($type == 'udemy')
    @section('cardsubtitle', 'The accounts which are provided by Udemy ')

@else
    @section('cardsubtitle', 'The accounts which are provided by JungleScout ')
@endif
@section('body')

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">@yield('cardtitle')</h4>
                    <p class="card-category">@yield('cardsubtitle')</p>
                </div>
                <div class="card-body">

                    <section class="row">
                        <div class="col-md-12">
                            <table class="table">
                                @php ($i = 1)
                                <thead>
                                <th>#</th>
                                <th>Email</th>
                                <th>Token</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                @foreach($udemy as $js)
                                    <tr class="{{ ($js->is_active) ? '' : 'bg-danger' }}">
                                        <td>{{$i++}}</td>
                                        <td>{{$js->email}}</td>
                                        <td>{{substr($js->token, 0, 20)."..."}}</td>
                                        <td>{{($js->is_active) ? "active" : "inactive"}}</td>
                                        <td class="td-actions text-right">

                                            <button type="button" rel="tooltip" title="Add Session" class="btn btn-danger btn-link btn-sm">
                                                <a class="" href="{{route('admin.udemy.token')}}/{{$js->id}}">
                                                    <i class="material-icons">launch</i>
                                                </a>
                                            </button>



                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-link btn-sm">
                                                <a class="tokendelete" href="{{route('admin.delete.udemy.token')}}/{{$js->id}}">
                                                    <i class="material-icons">close</i>
                                                </a>
                                            </button>


                                        </td>

                                    </tr>
                                @endforeach

                            </table>

                        </div>

                    </section>
                </div>
            </div>
        </div>


        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Add Udemy Account</h4>
                    <p class="card-category">Insert Valid Credentails</p>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.udemy.add.account')}}" method="POST" >
                        <label class="bmd-label-floating">Account Email</label>
                        <input type="email" name="email" class="form-control">

                        <label class="bmd-label-floating">Password</label>
                        <input type="password" name="password" class="form-control">
                        {{csrf_field()}}

                        <label class="bmd-label-floating">Token</label>
                        <textarea class="form-control" name="sess" placeholder="json value"></textarea>
                        <input type="submit" class="btn btn-primary" value="Add Udemy Account">
                    </form>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>


    </div>
    @if(isset($updateSession))
        <div class="row">

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header card-header-primary">
                        <h4 class="card-title ">Update Udemy Web Session</h4>
                        <p class="card-category">Login to original Udemy and copy the token.</p>
                    </div>
                    <div class="card-body">

                        <section class="row">
                            <div class="col-md-12">
                                <form action="{{route('admin.add.udemy.token')}}" method="post">
                                    <textarea rows=15 class="form-control" name="session">{{$content}}</textarea>
                                    {{csrf_field()}}
                                    <input type="hidden" name="u_id" value="{{$u_id}}">
                                    <input type="submit" class="btn btn-primary" value="Update Token">

                                </form>
                            </div>
                        </section>
                    </div>
                    @endif
                    <script>

                        $('.tokendelete').on('click', function(e){
                            var c = confirm("If you delete this account, all LOCAL ACCOUNT associated with the account will not be able to access Udemy. Are you sure you want to continue?");
                            if(!c) {
                                e.preventDefault();
                            }
                            // alert('ok');
                        });

                    </script>
@endsection
