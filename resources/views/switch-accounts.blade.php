@extends('dashboard')
@section('title', 'JS Account Switcher')
@section('cardtitle', 'Accounts Switch')
@section('cardsubtitle', 'This feature will update parent JS account of all local accounts with the selected ones.')
@section('body')

<div class="row">
    <div class="col-md-12">
        <div class="card">
        <div class="card-header card-header-primary">
            <h4 class="card-title ">@yield('cardtitle')</h4>
            <p class="card-category">@yield('cardsubtitle')</p>
        </div>
        <div class="card-body">

        <section class="row">
            <div class="col-md-12">
                <form action="{{ route('admin.doswitch')}}" method="POST">
                <table class="table">
                    <tr>
                        <td><strong>Switch From</strong></td>
                        <td><strong>Switch To</strong></td>
                    </tr>
                    <tr>
                        <td>
                            <select name="from_id" class="btn btn-danger dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                @foreach($jsaccounts as $js)
                                    <option value="{{$js->id}}">{{$js->email}}</option>
                                @endforeach
                            </select>
                        </td>
                        

                        <td>
                            <select name="to_id" class="btn btn-success dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">
                                @foreach($jsaccounts as $js)
                                    <option value="{{$js->id}}">{{$js->email}}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" class="btn btn-warning" value="Change!" /> {{csrf_field()}}
                        </td>
                        <td></td>
                    </tr>
                </table>
                </form>

            </div>
        </section>
        </div>
        </div>
    </div>
    
    

</div>
@endsection
