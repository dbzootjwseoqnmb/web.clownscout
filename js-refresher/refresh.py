import mysql.connector
import requests
import base64
import redis
import random
import time
import json
from concurrent.futures import ThreadPoolExecutor, as_completed

redis_client = redis.Redis(host='cdn.redis.amzpremiumtools.com', port=6300, db=0, password='Flannels3179')

def fetch_standard_listed_proxies():
#     proxy_list = """176.113.73.104:3128
#         176.113.73.99:3128
#         67.205.190.164:8080
#         46.21.153.16:3128
#         84.17.35.129:3128
#         104.248.59.38:80
#         12.156.45.155:3128
#         176.113.73.102:3128
#         142.11.222.22:80
#         107.178.9.186:8080
#         34.29.41.58:3128
#         137.184.197.190:80
#         174.126.217.110:80
#         38.51.49.84:999
#         143.42.194.37:3128
#         66.63.168.119:8000
#         52.41.249.10:80
#         67.22.28.62:8080
#         8.242.178.5:999
#         43.153.66.118:80
#         3.128.142.113:80
#         108.161.128.43:80
#         24.172.34.114:49920
#         155.50.208.37:3128
#         64.225.4.63:9993
#         155.50.241.99:3128
#         34.135.166.24:80
#         34.75.202.63:80
#         209.145.60.213:80
#         159.203.104.153:8200
#         50.113.36.155:8080
#         162.144.236.128:80
#         155.50.213.149:3128
#         45.61.187.67:4009
#         3.143.37.255:80
#         184.72.36.89:80
#         24.230.33.96:3128
#         142.147.114.50:8080
#         52.24.80.166:80
#         155.50.215.37:3128
#         109.122.195.16:80
#         157.230.226.230:1202
#         167.99.174.59:80
#         191.101.1.116:80
#         192.81.128.182:8089
#         68.183.143.134:80
#         38.56.70.97:999
#         37.120.222.132:3128
#         89.249.65.191:3128
#         144.91.118.176:3128
#         85.214.94.28:3128
#         167.172.109.12:39452
#         95.111.226.235:3128
#         167.172.109.12:40825
#         185.189.112.157:3128
#         185.189.112.133:3128
#         85.214.244.174:3128
#         167.172.109.12:41491
#         167.172.109.12:39533
#         167.172.109.12:46249
#         88.99.10.252:1080
#         167.172.109.12:37355
#         82.165.105.48:80
#         144.91.106.93:3128
#         188.34.164.99:8080
#         46.101.102.134:3128
#         116.203.27.109:80
#         207.180.250.238:80
#         88.99.148.60:8111
#         141.147.33.121:80
#         46.101.160.223:80
#         207.180.252.117:2222
#         78.47.103.89:8080
#         18.195.164.53:7777
#         159.69.214.139:3128
#         206.189.130.107:8080
#         15.207.196.77:3128
#         103.76.253.66:3129
#         103.174.102.127:80
#         103.155.54.26:83
#         103.51.21.250:83
#         103.78.170.13:83
#         182.72.203.246:80
#         103.125.154.233:8080
#         103.243.114.206:8080
#         103.134.165.38:8080
#         59.92.70.176:3127
#         103.242.119.88:80
#         103.206.208.135:55443
#         103.180.73.107:8080
#         103.160.207.49:32650
#         103.82.157.102:8080
#         13.229.107.106:80
#         13.229.47.109:80
#         119.81.71.27:80
#         119.81.71.27:8123
#         193.56.255.179:3128
#         139.180.140.254:1080
#         104.248.146.99:3128
#         18.141.177.23:80
#         193.56.255.181:3128
#         188.166.252.135:8080
#         34.87.103.220:80
#         34.126.187.77:80
#         165.154.236.214:80
#         51.79.249.186:3128
#         18.142.81.218:80
#         156.67.217.159:80
#         79.122.230.20:8080
#         46.160.129.189:3128
#         85.172.0.30:8080
#         194.186.35.70:3128
#         185.174.137.30:3128
#         92.255.205.129:8080
#         91.213.249.200:80
#         82.146.37.145:80
#         77.91.74.77:80
#         94.26.241.120:8080
#         194.186.127.60:80
#         77.233.5.68:55443
#         85.235.184.186:3129
#         46.0.203.186:8080
#         90.154.124.211:8080
#         149.126.101.162:8080
#         51.158.68.133:8811
#         51.158.68.68:8811
#         159.8.114.37:80
#         35.180.188.216:80
#         51.158.172.165:8811
#         159.8.114.37:8123
#         146.59.243.214:80
#         31.207.38.66:80
#         46.105.35.193:8080
#         81.250.223.126:80
#         51.159.134.210:3128
#         54.36.81.217:8080
#         5.135.136.60:9090
#         51.178.165.36:3128
#         37.187.88.32:8001
#         119.81.189.194:80
#         119.81.189.194:8123
#         193.239.86.249:3128
#         193.239.86.247:3128
#         193.239.86.248:3128
#         43.132.184.228:8181
#         202.162.105.202:8000
#         20.187.77.5:80
#         165.154.224.14:80
#         223.18.60.191:8080
#         202.162.105.202:80
#         103.234.55.173:80
#         123.202.159.108:80
#         20.205.115.87:8080
#         119.237.43.106:80
#         222.129.38.21:57114
#         106.45.221.168:3256
#         113.121.240.114:3256
#         121.206.205.75:4216
#         115.221.242.131:9999
#         125.87.82.86:3256
#         183.164.254.8:4216
#         116.242.89.230:3128
#         119.84.215.127:3256
#         113.195.224.222:9999
#         112.98.218.73:57658
#         223.113.89.138:1080
#         36.7.252.165:3256
#         113.100.209.184:3128
#         43.251.119.79:45787
#         37.120.133.137:3128
#         84.17.51.235:3128
#         84.17.51.241:3128
#         84.17.51.240:3128
#         109.111.212.78:8080
#         143.198.241.47:80
#         178.128.172.154:3128
#         134.209.189.42:80
#         209.250.230.101:9090
#         213.171.214.19:8001
#         46.101.19.131:80
#         80.194.38.106:3333
#         92.207.253.226:38157
#         141.136.42.164:80
#         81.12.119.171:8080
#         217.172.122.14:8080
#         5.78.89.192:8080
#         45.159.150.23:3128
#         217.219.74.130:8888
#         5.78.44.6:8080
#         91.107.203.75:8080
#         193.176.242.186:80
#         89.43.10.141:80
#         31.214.171.62:3128
#         185.118.155.202:8080
#         46.209.54.102:8080
#         84.241.8.234:8080
#         152.231.25.114:8080
#         201.182.251.142:999
#         181.57.131.122:8080
#         190.69.157.213:999
#         179.1.192.17:999
#         179.1.133.33:999
#         170.239.207.241:999
#         181.129.43.3:8080
#         177.93.44.53:999
#         190.71.24.129:999
#         186.159.6.163:1994
#         201.219.201.14:999
#         181.205.41.210:7654
#         180.183.97.16:8080
#         14.207.24.176:8080
#         180.180.218.250:8080
#         1.179.148.9:55636
#         183.88.184.48:8080
#         125.25.40.41:32650
#         1.2.252.65:8080
#         183.89.41.224:8080
#         181.212.45.226:8080
#         181.74.81.195:999
#         181.212.41.171:999
#         186.103.130.91:8080
#         146.83.118.9:80
#         190.110.99.189:999
#         45.225.184.177:999
#         200.54.194.13:53281
#         161.202.226.194:8123
#         103.151.41.7:80
#         163.44.253.160:80
#         140.83.32.175:80
#         150.230.207.167:80
#         103.42.28.27:45787
#         150.230.59.34:8080
#         190.111.209.207:3128
#         45.238.12.4:3128
#         143.202.97.171:999
#         168.90.255.60:999
#         190.136.50.67:3128
#         170.210.121.190:8080
#         200.106.184.97:999
#         188.166.30.17:8888
#         79.110.52.252:3128
#         94.100.18.111:3128
#         178.62.229.28:3128
#         84.241.188.138:8111
#         45.90.104.150:9090
#         95.216.17.79:3888
#         95.217.104.21:24815
#         95.217.195.146:9999
#         95.216.230.239:80
#         95.217.137.46:8080
#         65.21.131.27:80
#         185.123.101.174:3128
#         94.231.192.97:8080
#         81.161.236.152:8080
#         88.255.102.123:8080
#         78.188.81.57:8080
#         185.200.37.98:8080
#         203.243.63.16:80
#         13.209.156.241:80
#         121.139.218.165:31409
#         52.79.107.158:8080
#         61.111.38.5:80
#         125.141.151.83:80
#         102.216.69.176:8080
#         154.79.254.236:32650
#         102.0.0.118:80
#         196.202.210.73:32650
#         197.232.36.85:41890
#         41.217.223.145:32650
#         154.239.9.94:8080
#         41.65.160.171:1981
#         41.65.55.10:1976
#         41.33.66.228:1981
#         195.246.54.31:8080
#         156.200.116.71:1981
#         190.238.231.65:1994
#         181.65.169.35:999
#         190.116.2.52:80
#         201.218.144.18:999
#         190.187.201.26:8080
#         201.218.144.19:999
#         119.93.129.34:80
#         143.44.191.108:8080
#         122.52.196.36:8080
#         58.69.201.117:8082
#         112.205.92.14:8080
#         187.141.184.235:8080
#         45.188.166.52:1994
#         45.231.221.193:999
#         189.250.135.40:80
#         177.229.210.50:8080
#         159.89.113.155:8080
#         64.56.150.102:3128
#         172.105.107.223:3128
#         51.222.155.142:80
#         190.113.40.202:999
#         201.229.250.21:8080
#         45.229.34.174:999
#         190.89.37.73:999
#         185.123.143.251:3128
#         185.123.143.247:3128
#         37.120.140.158:3128
#         191.97.16.160:999
#         201.249.152.172:999
#         191.97.19.66:999
#         102.38.17.193:8080
#         41.254.53.70:1981
#         102.38.22.121:8080
#         85.221.249.213:8080
#         194.31.53.250:80
#         153.19.91.77:80
#         43.255.113.232:84
#         203.189.150.48:8080
#         103.216.51.36:32650
#         200.24.130.138:999
#         157.100.6.202:999
#         181.39.27.225:1994
#         45.235.123.45:999
#         190.128.241.102:80
#         181.120.28.228:80
#         118.99.108.4:8080
#         103.114.53.2:8080
#         169.57.157.148:80
#         169.57.157.146:8123
#         3.24.178.81:80
#         3.24.58.156:3128
#         185.236.203.208:3128
#         146.70.80.76:80
#         185.236.202.205:3128
#         185.236.202.170:3128
#         193.34.95.110:8080
#         92.118.132.125:8080
#         103.28.121.58:3128
#         103.28.121.58:80
#         185.38.111.1:8080
#         212.192.31.37:3128
#         190.5.77.211:80
#         200.52.148.10:999
#         139.5.73.71:8080
#         103.153.232.41:8080
#         103.176.179.84:3128
#         222.255.238.159:80
#         36.229.100.73:80
#         61.216.156.222:60808
#         81.94.255.13:8080
#         85.238.74.91:8080
#         151.22.181.205:8080
#         156.54.240.53:3128
#         81.44.83.70:8080
#         41.204.63.118:80
#         190.61.88.147:8080
#         103.137.91.250:8080
#         41.86.46.112:8080
#         213.6.155.9:19000
#         41.86.252.91:443
#         16.170.1.8:80
#         154.72.90.74:8081
#         200.108.197.2:8080
#         154.113.121.60:80
#         41.77.188.131:80
#         190.186.237.103:80
#         196.1.95.124:80
#         175.139.179.65:42580
#         134.19.254.2:21231
#         122.129.84.12:8080
#         103.102.85.1:8080
#         80.91.125.238:8089
#         102.39.68.76:8080
#         """
#     qcejbwpq-rotate:bolmvwpux9f6@p.webshare.io:80
#     brd-customer-hl_2592d671-zone-data_center:oz6jeby04pp8@brd.superproxy.io:22225
    proxy_list = "127.0.0.1:8080"
#     proxy_list = "admin:gNrTnLJcP98CMw@149.102.128.51:8893"
#     proxy_list = "brd-customer-hl_2592d671-zone-data_center:oz6jeby04pp8@brd.superproxy.io:22225"
    return proxy_list

def fetch_fresh_proxies():
    proxy_list_url = 'https://raw.githubusercontent.com/TheSpeedX/PROXY-List/master/http.txt'
    proxy_list_response = requests.get(proxy_list_url, verify=False)

    if proxy_list_response.status_code != 200:
        raise Exception("Failed to fetch the proxy list.")
    return proxy_list_response.text
def proxies():
    proxy_list = fetch_standard_listed_proxies()
#     proxy_list = fetch_fresh_proxies()
    _proxies = proxy_list.split("\n")

    random_proxy = random.choice(_proxies)
    return random_proxy.strip()


def get_token_health(js_file=""):
    if js_file:
        # Check if the token file exists in Redis
        if not redis_client.exists(js_file):
            return -2  # Token file does not exist

        token = redis_client.get(js_file).decode()

        url = "https://ext.junglescout.com/api/v1/opportunity_score"
        data = {
            "products": [
                {
                    "estSales": 0,
                    "reviews": 1558,
                    "lqs": 0
                }
            ]
        }

        daily_token = json.loads(base64.b64decode(token).decode())
#         print(daily_token['daily_token'])
        if not daily_token["status"]:
            return 2

        headers = get_header()
#         headers["Authorization"] = f"Bearer {daily_token['daily_token'].replace('\n', '')}"
        headers["Authorization"] = "Bearer " + daily_token["daily_token"].replace("\n", "")

        auth_data = send_post_request(url, data, headers)
        status = auth_data.json()
        print(status)
        return 0 if status["code"] == -1 else 1


def get_header():
    header = {
        'Host': 'ext.junglescout.com',
        'Accept': 'application/json, text/javascript, */*; q=0.01',
        'Authorization': '',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63',
        'Content-Type': 'application/json',
        'Origin': 'chrome-extension://hbepkiiehjkmcoghfeffaepdhddbkkol',
        'Sec-Fetch-Dest': 'empty',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'en-US,en;q=0.9',
        'X-Datadome-Clientid': 'h767NOMB84851hoOCRpnY41nbRkcx1muR5bQ8AcrwaHMpDvn526G7u9fo0h8IPsQAVI8Hf4qterdCy434NLuVUrl5rpJx__gB2mn7VzkwtL~r6EddnKcvJp6~Rd2KK0S; _uetsid=3dce277071e111eeb365750d4e765f27',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-Mode': 'cors'
    }
    return header


def get_fresh_token(email, password, file):
    auth_url = 'https://ext.junglescout.com/api/v1/users/initial_authentication'
    data = {
        'username': email,
        'password': password,
        'app': 'jsp'
    }

    headers = get_header()
    auth_response = send_post_request(auth_url, data, headers)
    auth_data = auth_response.json()  # Parse the JSON content of the response
    return auth_data

def send_get_request(url):
    ip = proxies()
    http_proxies = {
        'http': 'http://' + str(ip),
        'https': 'http://' + str(ip),
    }
    print(http_proxies)
    response = requests.get(url, proxies=http_proxies, verify=False)
    print("proxy ip:" + str(ip), "response ip: " +response.text)
def send_post_request(url, data, header):
    ip = proxies()
    _proxies = {
        'http': 'http://' + str(ip),
        'https': 'http://' + str(ip),
    }
#     print(ip)
    try:
        response = requests.post(url, json=data, headers=header, proxies=_proxies, verify=False)
        return response
    except Exception as e:
        print(f"Request error: {e}")
def store_token(token, file):
#     print(token.content)
    encoded = base64.b64encode(json.dumps(token).encode()).decode()  # Encode the content of the Response object
    redis_client.set(file, encoded)

def refresh_tokens(js_email, js_password, js_file):
    status = 2
    try:
        status = get_token_health(js_file)
    except Exception as e:
        print(f"status not found trying again: {e}")
        return
    if status in [0, -2]:
        try:
            fresh_token = get_fresh_token(js_email, js_password, js_file)
            store_token(fresh_token, js_file)
            #update_token_status(js_id, js_email, 1)
        except Exception as e:
            print(f"An error occurred for {js_email}: {e}")
    elif status == 2:
        print("ignoring the update")
        #update_token_status(js_id, js_email, 0)

def main():
    while True:
        try:
            # Connect to MySQL database
            db_connection = mysql.connector.connect(
                host="cdn.database.amzpremiumtools.com",
                user="web-amz",
                password="PZDJBGp8wCFySRTM",
                database="web-amz"
            )

            query = "SELECT * FROM jsaccounts"
            cursor = db_connection.cursor()
            cursor.execute(query)
            rows = cursor.fetchall()

    #         print(rows)
    #         return

            # Define the number of threads you want to run concurrently
            num_threads = 3
            for row in rows:
                email = row[1]
                password = row[2]
                token_file = row[3]
                print(email, password, token_file)
                refresh_tokens(email, password, token_file)
#             with ThreadPoolExecutor(max_workers=num_threads) as executor:
#                 futures = []
#
#                 future = executor.submit(refresh_tokens, email, password, token_file)
#                 futures.append(future)
    #
#                 for future in as_completed(futures):
#                     future.exception()  # Handle any exceptions that occurred in the thread

            cursor.close()
            db_connection.close()

            print("Waiting for the next refresh...")
            time.sleep(10)
        except Exception as e:
            print(f"An error occurred for: {e}")
            time.sleep(20)


if __name__ == "__main__":
    main()
#     send_get_request("https://ipinfo.io/ip")
