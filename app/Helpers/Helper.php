<?php

function subscriptions($arr, $helium_id, $keepa_id){
    $text = "";
    foreach($arr as $value){
        if($value['type'] == 1) {
            $text .= "Web ";
        }
        if($value['type'] == 2) {
            $text .= "<br>Extension ";
        }
    }
    if($helium_id != 0) {
        $text .="<br>Helium";
    }
    if($keepa_id != 0) {
        $text .="<br>Keepa";
    }
    return $text;
}
