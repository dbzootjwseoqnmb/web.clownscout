<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keepa extends Model
{
    use HasFactory;
    protected $fillable = [
        'token',
        'email',
        'password',
        'is_active',
    ];
    protected $table = "keepa";
}
