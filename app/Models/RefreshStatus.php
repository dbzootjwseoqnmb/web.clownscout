<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefreshStatus extends Model
{
    use HasFactory;
    protected $fillable = [
        'js_id',
        'js_email',
        'type',
    ];

    //type => 1 => js extension
    //type => 2 => js web
    
    protected $table = "refresh_status";


}
