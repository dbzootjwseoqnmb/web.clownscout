<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\JsAccounts;
use App\Models\UserPivot;


class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'role',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function js(){
        return $this->belongsTo(JsAccounts::class, 'js_id');
    }
    public function roles(){
        return $this->hasMany(UserPivot::class, 'user_id');
    }
    public function helium(){
        return $this->belongsTo(Helium::class, 'helium_id');
    }
    public function keepa(){
        return $this->belongsTo(Keepa::class, 'keepa_id');
    }
    public function udemy(){
        return $this->belongsTo(Udemy::class, 'udemy_id');
    }
}
