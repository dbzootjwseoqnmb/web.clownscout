<?php

namespace App\Http\Controllers;
use App\Models\Keepa;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Udemy;
use App\Models\JsAccounts;
use App\Models\Helium;
use App\Models\LocalAccounts;
use App\Models\UserPivot;
use App\Models\RefreshStatus;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class ManagerController extends Controller
{
    //
    private $client;

    public function __construct(){
        $this->client = new \GuzzleHttp\Client(['verify' => false ]);
    }

    private function proxies(){
        $proxyListUrl = 'https://raw.githubusercontent.com/mertguvencli/http-proxy-list/main/proxy-list/data.txt';
        $proxyList = file_get_contents($proxyListUrl);
        if ($proxyList === false) {
            die("Failed to fetch the proxy list.");
        }

        // Step 2: Parse the list of proxies into an array
        $proxies = explode("\n", $proxyList);

        // Step 3: Randomly select one proxy from the array
        $randomProxy = $proxies[array_rand($proxies)];
        return $randomProxy;
    }
    public function test(){
//        Redis::set('test', 'working', 'NX');
//         echo Redis::get('test')."<br>";
        Redis::set('test3', 'working now is it?');
        echo Redis::get('test3');

//        echo \Session::getId();


//        $data = array(
//            'products' => array(
//                [
//                    'estSales' => 0,
//                    'reviews' => 1558,
//                    'lqs' => 0
//                ]
//            )
//
//        );
//        echo json_encode($data);
    }

    public function login()  {
        $title = "Login";
        return view("login")->with('title', $title);

    }
    
    public function testAccount(){
        $user = new User();
        $user->name = 'sys_user';
        $user->email = 'Usamarog@gmail.com';
        $user->password = \Hash::make('futurechef');
        $user->save();
    }
    private function storeToken($token, $file){
        $encoded = base64_encode($token);
//        $f = fopen(storage_path('app/'.$file), 'w+');
//        fwrite($f, $encoded);
//        fclose($f);
        Redis::set($file, $encoded);
//        echo Redis::get('test3');
    }
    public function refreshToken(Request $request){
        $id = $request->token;
        $js = JsAccounts::find($id);
        $token = $this->getFreshToken($js->email, $js->password, $js->file);
        $this->storeToken($token, $js->file);
        echo (json_decode($token)->status) ? "Token refreshed" : "Something is wrong check the password maybe";

    }

    private function getFreshToken($email, $password, $file) {
        $auth_url = 'https://ext.junglescout.com/api/v1/users/initial_authentication';
        $data = array(
            'username' => $email,
            'password' => $password,
            'app' => 'jsp'
        );

        $auth_data = $this->sendPostRequest($auth_url, $data, $this->getHeader());
        return $auth_data;
    }
    private function getHeader(){
        $header = array(
            'Host' => 'ext.junglescout.com',
            'Accept' => 'application/json, text/javascript, */*; q=0.01',
            'Authorization' => "",
            'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36 Edg/88.0.705.63',
            'Content-Type' => 'application/json',
            'Origin' => 'chrome-extension://hbepkiiehjkmcoghfeffaepdhddbkkol',
            'Sec-Fetch-Dest' => 'empty',
            'Accept-Encoding' => 'gzip, deflate',
            'X-Datadome-Clientid' => 'QeL3e1IHIzFSBAaVtij6PxmKi7IzusopGEM0qT9tE45X0VZG6TOlwOPxFkewZ3dZcBSI0OWb~eB1OS2MgZPVeZz2V29xtIzA4mctFQ50PIyEg6vG62jHiJHix2nZg52b',
            'Accept-Language' => 'en-US,en;q=0.9',
            'Sec-Fetch-Site' => 'none',
            'Sec-Fetch-Mode' => 'cors',

        );
        return $header;
    }
    private function sendGetRequest($url, $header) {
        $res = $this->client->request('GET', $url, [
            'headers' => $header,
            "proxy" => "http://admin:gNrTnLJcP98CMw@149.102.128.51:8893",
        ]);
        return $res->getBody();
    }

    public function deleteLocalAccount($local_id) {
        $js = User::find($local_id)->delete();
        UserPivot::where('user_id', $local_id)->delete();
        return redirect()->route('admin.scout.account.local');

    }
    public function keepaSessionReturn_1(){
        $currentUser = \Auth::user();
        return $currentUser->keepa->token;
    }

    public function udemySessionReturn(){
        $currentUser = \Auth::user();
        return $currentUser->udemy->token;
    }
    public function keepaSessionReturn(){
        $currentUser = \Auth::user();
        $token = explode(",", $currentUser->keepa->token);
        return $token[0];
    }
    public function webSessionReturn(){
        $currentUser = \Auth::user();
        $file = "_s_".$currentUser->js->file;
//        $session_file = storage_path('app/'.$file);
        return Redis::get($file);
    }

    public function heliumSession1Return(){
        $helium = Helium::find('999');
        return $helium->session;
    }
    public function heliumSession2Return(){
        $helium = Helium::find('998');
        return $helium->session;
    }
    public function heliumSessionReturn(){
        $currentUser = \Auth::user();

        return $currentUser->helium->session;
    }



    public function webTokenDisplay($js_id){
        // return $js_id;
        $jsaccounts = JsAccounts::all();
        $updateSession = true;
        $js = JsAccounts::find($js_id);
        $file = "_s_".$js->file;
//        $session_file = storage_path('app/'.$file);
        if(!Redis::exists($file)) {
            Redis::set($file, "");
        }
//        if(!is_file($session_file)) file_put_contents($session_file, "");
//        $content = file_get_contents($session_file);
        $content = Redis::get($file);
        return view('scout-account', compact('jsaccounts', 'updateSession', 'content', 'js_id'));
    }

    public function addWebToken(Request $request){
        $js_id = $request->js_id;
        $updateSession = true;
        $content = $request->session;
        $jsaccounts = JsAccounts::all();
        $js = JsAccounts::find($js_id);
        $file = "_s_".$js->file;
//        $session_file = storage_path('app/'.$file);
//        file_put_contents($session_file, "");
//        file_put_contents($session_file, $content);
        Redis::set($file, $content);

        return view('scout-account', compact('jsaccounts', 'updateSession', 'content', 'js_id'));
    }


    public function switchAccounts(){
        $jsaccounts = JsAccounts::all();
        return view('switch-accounts', compact('jsaccounts'));

    }
    public function switchAccountsDo(Request $request){
        $from = $request->from_id;
        $to = $request->to_id;
        $users = User::where('js_id', $from)->update(['js_id' => $to]);
        // foreach($users as $user) {
        //     $user->js_id = $to;
        //     $user->save();
        // }
        return redirect()->route('admin.scout.account.local');


    }
    public function deleteKeepaAccount($token){
        $lcs = User::where('keepa_id', $token);
        foreach($lcs as $localuser){
            $localuser->keepa_id = 0;
            $localuser->save();
        }
        $js = Helium::find($token)->delete();
        // $js->
        return redirect()->route('admin.keepa.account');
    }

    public function deleteHeliumAccount($token){
        $lcs = User::where('helium_id', $token);
        foreach($lcs as $localuser){
            $localuser->helium_id = 0;
            $localuser->save();
        }
        $js = Helium::find($token)->delete();
        // $js->
        return redirect()->route('admin.helium.account');
    }
    public function deleteAccount($token){

        // echo "We are okay";
        $lcs = User::where('js_id', $token)->delete();
        $js = JsAccounts::find($token)->delete();
        // $js->
        return redirect()->route('admin.scout.account');

    }
    private function sendPostRequest($url, $data, $header){

//        $request = $this->client->post($url, $header);
//        $request->setBody(json_encode($data));
//        $response = $request->send();
//        dd($response);

//        dd(json_encode($data));

        $res = $this->client->request('POST', $url, [
            "body" => json_encode($data),
            'headers' => $header,
            "proxy" => "http://admin:gNrTnLJcP98CMw@149.102.128.51:8893"
           // "proxy" => "http://cdjtfrpr-rotate:9kj9m7yoqqju@p.webshare.io:80/",
//            "proxy" => "http://".$this->proxies(),
//            "proxy" => "http://84.38.160.80:8080"
        ]);
//        $res->setBody(json_encode($data));
        return $res->getBody();
    }
    public function pageNotFound() {
        return view('404');
    }
    public function dologin(Request $request) {
        if($this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            ])) {
                if (\Auth::attempt([
                    'email' => $request->email,
                    'password' => $request->password])
                ){
                    // dd(\Session::getId());
                    $user = \Auth::user();

                    if($user->role == "user"){

                        $last_login = $user->updated_at;
                        $current_time = Carbon::now();
                        $difference = $current_time->diffInSeconds($last_login);
                        // dd();
                        //account lockout time for 5 minutes
                        if($user->last_session != "" && ((300-$difference) > 2)){
                            // dd();
                            \Auth::logout();
                            return redirect()->route('login')->with('error', 'User already logged in.');
                        }

                        $user->last_session = \Session::getId();
                        $user->update();
                        return redirect()->route('user.dashboard');
                    }
                    return redirect()->route('admin.dashboard');
                }
                return redirect()->route('login')->with('error', 'Invalid email address or password');
            } else {
                return redirect()->route('login')->with('error', 'Invalid email');
            }

    }

    public function dashboard(){


        return view("dashboard");
    }


    public function createAccount(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            ]);
        $user = new User();
        $user->name = 'sys_user';
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();
    }
    private function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function addKeepaToken(Request $request){
        $js_id = $request->js_id;
        $type = 'keepa';
        $updateSession = true;
        $content = $request->session;
        $js = Keepa::find($js_id);
        $js->token = $content;
        $js->save();
        $keepa = keepa::all();
        return view('keepa-account', compact('keepa', 'updateSession', 'content', 'js_id', 'type'));
    }
    public function addHeliumToken(Request $request){
        $js_id = $request->js_id;
        $type = 'helium';
        $updateSession = true;
        $content = $request->session;
        if($content != ""){
            $updateContent = json_decode($content);
            $cleanContent = array();
            foreach($updateContent as $c ){
                if($c->name == "current-marketplace") {
                    continue;
                }
                array_push($cleanContent, $c);
            }
            $content = json_encode($cleanContent);
        }

        $js = Helium::find($js_id);
        $js->session = $content;
        $js->save();
        $helium = Helium::all();
        return view('helium-account', compact('helium', 'updateSession', 'content', 'js_id', 'type'));
    }
    public function addKeepa(Request $request) {
        $js = new Keepa();
        $js->email = $request->email;
        $js->password = $request->password;
        $content = $request->sess;
        $js->token = $content;
        $js->is_active = 1;
        $js->save();
        return redirect()->route('admin.keepa.account');
    }

    public function addHelium(Request $request) {
        $js = new Helium();
        $js->email = $request->email;
        $js->password = $request->password;
        $content = $request->sess;
        $updateContent = json_decode($content);
        $cleanContent = array();
        foreach($updateContent as $c ){
            if($c->name == "current-marketplace") {
                continue;
            }
            array_push($cleanContent, $c);
        }
        $content = json_encode($cleanContent);

        $js->session = $content;
        $js->status = true;
        $js->save();
        return redirect()->route('admin.helium.account');
    }
    public function addScout(Request $request) {
        $file_name = $this->generateRandomString() .".txt";
        $js = new JsAccounts();
        $js->email = $request->email;
        $js->password = $request->password;
        $js->file = $file_name;
        $js->status = true;
        $js->save();
        return redirect()->route('admin.scout.account');
    }
    public function scout(Request $request){
        //displays js account already placed
        $jsaccounts = JsAccounts::all();
        $type = 'js';
         return view('scout-account', compact('jsaccounts', 'type'));
    }


    public function keepaTokenDisplay($keepa_id){
        // return $js_id;
        $keepa = Keepa::all();
        $updateSession = true;
        $type = 'keepa';
        $js = Keepa::find($keepa_id);
        $content = $js->token;
        $js_id = $keepa_id;
        return view('keepa-account', compact('keepa', 'updateSession', 'js_id', 'type', 'content'));
    }
    public function heliumTokenDisplay($js_id){
        // return $js_id;
        $helium = Helium::all();
        $updateSession = true;
        $type = 'helium';
        $js = Helium::find($js_id);
        $content = $js->session;
        return view('helium-account', compact('helium', 'updateSession', 'js_id', 'type', 'content'));
    }

    public function keepa(Request $request){
        $keepa = Keepa::all();
        $type = 'keepa';
        return view("keepa-account", compact('keepa', 'type'));
    }

    public function helium(Request $request){
        $helium = Helium::all();
        $type = 'helium';
        return view("helium-account", compact('helium', 'type'));
    }
    public function adminAccounts() {
        $isAdmin = true;
        $jsaccounts = JsAccounts::all();
        $localaccount = User::where(['role'=> 'admin'])->get();
        // dd($localaccount->js->email);
        return view('localaccount', compact('jsaccounts', 'localaccount', 'isAdmin'));
    }
    public function localClown(Request $request) {
        $jsaccounts = JsAccounts::all();
        $helium = Helium::all();
        $keepa = Keepa::all();
        $udemy = udemy::all();
        if($request->search){
            $localaccount = User::where(['role'=> 'user'])->where('name', 'LIKE', '%'.$request->search."%")->simplePaginate(100);

        }
        else if($request->email){
            $localaccount = User::where(['role'=> 'user'])->where('email', 'LIKE', '%'.$request->email."%")->simplePaginate(100);
        }
        else {
            $localaccount = User::where(['role'=> 'user'])->simplePaginate(100);
        }
        $localcount = User::where(['role'=> 'user'])->count();
        // dd($localaccount->js->email);
        return view('localaccount', compact('jsaccounts', 'helium', 'keepa', 'udemy', 'localaccount', 'localcount'));
    }
    public function addAdminAccounts(Request $request){
        $localaccount = new User();
        $localaccount->name = $request->name;
        $localaccount->email = $request->email;
        $localaccount->role = "admin";
        $localaccount->password = \Hash::make($request->password);
        $localaccount->save();
        return redirect()->route('admin.admins');
    }
    public function addLocalClown(Request $request) {
        $localaccount = new User();
        $localaccount->name = $request->name;
        $localaccount->js_id = $request->js_id;
        $localaccount->helium_id = $request->helium_id;
        $localaccount->keepa_id = $request->keepa_id;
        $localaccount->udemy_id = $request->udemy_id;
        $localaccount->email = $request->email;
        $localaccount->role = "user";
        $localaccount->password = \Hash::make($request->password);
        $localaccount->save();

        if($request->js_id != 0){
            foreach($request->subscriptions as $value){
                $subscriptions = new UserPivot();
                $subscriptions->user_id = $localaccount->id;
                $subscriptions->type = $value;
                $subscriptions->save();
            }
        }

        return redirect()->route('admin.scout.account.local');
    }
    public function logout(Request $request)
    {
        if(\Auth::check())
        {
            \Auth::logout();
            $request->session()->invalidate();
        }
        return  redirect('panel/login');
    }
    private function getTokenHealth($jsfile = ""){

        //{"status":false,"message":"Invalid Token","code":-1}

        if($jsfile != ""){
//            $filename = storage_path('app/'.$jsfile);
//            if(!file_exists($filename))
//                return -2; //token file does not exist
//            $f = fopen($filename, 'r');
//            $token = fread($f, filesize($filename));
//            fclose($f);

            if(!Redis::exists($jsfile)) {
                return -2; //token file does not exist
            }
            $token = Redis::get($jsfile);
        }

        $url = "https://ext.junglescout.com/api/v1/opportunity_score";
        $data = array(
            'products' => array(
                [
                    'estSales' => 0,
                    'reviews' => 1558,
                    'lqs' => 0
                ]
            )

        );
        $daily_token = json_decode(base64_decode($token));
        if($daily_token->status == false) {
            return 2;
        }
        // dd($daily_token);
        $headers = $this->getHeader();
        $headers['Authorization'] = " Bearer ".str_replace("\n", "", $daily_token->daily_token);
//        dd($headers);

        $auth_data = $this->sendPostRequest($url, $data, $headers);
//        dd($auth_data);
        $status = json_decode($auth_data);
//        dd($status);
        return ($status->code == -1 ) ? 0 : 1;
    }
    public function autoRefreshToken(){
        $jsaccounts = JsAccounts::all();
        foreach($jsaccounts as $js) {
            $status = $this->getTokenHealth($js->file);
            if($status === 0 || $status === -2){
                //0 -> needs to be refreshed
                //-2 -> some error in file reading
                //1 -> token is working fine
                //2 -> token is at false status meaning password isn't correct!

                $token = $this->getFreshToken($js->email, $js->password, $js->file);
                $this->storeToken($token, $js->file);
                $status = new RefreshStatus();
                $status->js_id = $js->id;
                $status->js_email = $js->email;
                $status->type = 1;
                $status->save();
                $js->status = 1;
                $js->save();


                //type => 1 => js extension
                //type => 1 => js web
            } elseif ($status === 2){
                $js->status = 0;
                $js->save();
            }
//            sleep(10);

        }

    }


    public function udemy(Request $request){
        $udemy = Udemy::all();
        $type = 'udemy';
        return view("udemy-account", compact('udemy', 'type'));
    }


    public function addUdemy(Request $request) {
        $js = new Udemy();
        $js->email = $request->email;
        $js->password = $request->password;
        $content = $request->sess;
        $js->token = $content;
        $js->is_active = 1;
        $js->save();
        return redirect()->route('admin.udemy.account');
    }


    public function udemyTokenDisplay($udemy_id){
        // return $js_id;
        $udemy = Udemy::all();
        $updateSession = true;
        $type = 'udemy';
        $js = Udemy::find($udemy_id);
        $content = $js->token;
        $u_id = $udemy_id;
        return view('udemy-account', compact('udemy', 'updateSession', 'u_id', 'type', 'content'));
    }

    public function addUdemyToken(Request $request){
        $u_id = $request->u_id;
        $type = 'udemy';
        $updateSession = true;
        $content = $request->session;
        $u = Udemy::find($u_id);
        $u->token = $content;
        $u->save();
        $udemy = Udemy::all();
        return view('udemy-account', compact('udemy', 'updateSession', 'content', 'u_id', 'type'));
    }

    public function deleteUdemyAccount($token){
        $lcs = User::where('udemy_id', $token);
        foreach($lcs as $localuser){
            $localuser->udemy_id = 0;
            $localuser->save();
        }
        $js = Udemy::find($token)->delete();
        // $js->
        return redirect()->route('admin.udemy.account');
    }
}
