<?php

namespace App\Http\Middleware;

use Closure;
// use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
class RoleUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = "user";
        $user = \Auth::user();
        if($user->role == $role) {
            if($user->last_session != \Session::getId()) {
                \Auth::logout();
                return redirect()->route('login')->with('error', 'Someone else logged in!');
            }
            return $next($request);
        }
        
    
        return redirect('/404');
    }
}