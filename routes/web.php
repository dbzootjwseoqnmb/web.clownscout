<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ScoutController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Auth::routes();

Route::get('/panel/test/token/refresh/automatic', "ManagerController@autoRefreshToken");
Route::get('/', "ManagerController@login");
Route::get('login', "ManagerController@login")->name("login");
Route::post("login", "ManagerController@doLogin")->name("doLogin");
Route::get('/404', function () {
    return view('404');
});
Route::get('logout', function(){
    \Auth::logout();
    return redirect()->route('login');
})->name('logout');
Route::get('ext/logout/info', function(){
    if(auth()->check()){
        $data = [];
        $user = auth()->user();
        $js = $user->js_id;
        $helium = $user->helium_id;
        if($js != null) {
            $data['js'] = true;
        } else
            $data['js'] = false;
        ($helium !== 0) ? $data['helium'] = true : $data['helium'] = false;
        echo json_encode($data);
    } else {
        $data = [];
        $data['js'] = false;
        $data['helium'] = false;
        echo json_encode($data);
    }
});
Route::get('ext/login/check', function(){
    if(\Auth::check()){
        $user = auth()->user();
        return 1;
    }
    return 0;
});
// Route::get('/test', 'ManagerController@test');
// Route::get('test', "ManagerController@testAccount");

Route::group(['prefix'=>'panel','as'=>'panel.'], function(){
    // Route::get('login', "ManagerController@login");
    // Route::post('login', "ManagerController@dologin");
    // Route::get('test', "ManagerController@testAccount");

});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['auth', 'RoleAdmin']], function(){
    Route::get('dashboard',"ManagerController@dashboard")->name('dashboard');
    Route::get('scout/account', "ManagerController@scout")->name('scout.account');
    Route::get('helium/account', "ManagerController@helium")->name('helium.account');
    Route::post('scout/add/account', "ManagerController@addScout")->name('scout.add.account');
    Route::post('helium/add/account', "ManagerController@addHelium")->name('helium.add.account');
    Route::get('scout/account/local', "ManagerController@localClown")->name('scout.account.local');
    Route::post('scout/account/local', "ManagerController@addLocalClown")->name('scout.add.account.local');
    Route::get('scout/refresh/token/', "ManagerController@refreshToken")->name('refresh.token');
    Route::get('scout/refresh/token/{token}', "ManagerController@refreshToken")->name('refresh.token.update');
    Route::get('scout/delete/token', 'ManagerController@deleteAccount')->name('delete.token');
    Route::get('helium/delete/token', 'ManagerController@deleteHeliumAccount')->name('delete.helium.token');
    Route::get('scout/delete/token/{token}', 'ManagerController@deleteAccount')->name('delete.token.action');
    Route::get('helium/delete/token/{token}', 'ManagerController@deleteHeliumAccount')->name('delete.helium.token.action');
    Route::get('scout/delete/local/', 'ManagerController@deleteLocalAccount')->name('delete.local');
    Route::get('scout/delete/local/{local_id}', 'ManagerController@deleteLocalAccount')->name('delete.local.action');
    Route::get('scout/add/token', 'ManagerController@webTokenDisplay')->name('web.token');
    Route::get('helium/add/token', 'ManagerController@heliumTokenDisplay')->name('helium.token');
    Route::get('helium/add/token/{js_id}', 'ManagerController@heliumTokenDisplay');
    Route::get('scout/add/token/{js_id}', 'ManagerController@webTokenDisplay');
    Route::post('scout/insert/token', 'ManagerController@addWebToken')->name("add.web.token");
    Route::post('helium/insert/token', 'ManagerController@addHeliumToken')->name("add.helium.token");
    Route::get('scout/admins', 'ManagerController@adminAccounts')->name('admins');
    Route::post('scout/admins', 'ManagerController@addAdminAccounts')->name('add.admins');
    Route::get('scout/switch', 'ManagerController@switchAccounts')->name('switch');
    Route::POST('scout/switch', 'ManagerController@switchAccountsDo')->name('doswitch');
    Route::get('test', "ManagerController@test");


    Route::get('keepa/delete/token', 'ManagerController@deleteKeepaAccount')->name('delete.keepa.token');
    Route::get('keepa/account', "ManagerController@keepa")->name('keepa.account');
    Route::post('keepa/add/account', "ManagerController@addKeepa")->name('keepa.add.account');
    Route::post('keepa/insert/token', 'ManagerController@addKeepaToken')->name("add.keepa.token");
    Route::get('keepa/add/token', 'ManagerController@keepaTokenDisplay')->name('keepa.token');
    Route::get('keepa/add/token/{keepa_id}', 'ManagerController@keepaTokenDisplay');

    Route::get('udemy/account', "ManagerController@udemy")->name('udemy.account');
    Route::post('udemy/add/account', "ManagerController@addUdemy")->name('udemy.add.account');
    Route::get('udemy/add/token/{udemy_id}', 'ManagerController@udemyTokenDisplay');
    Route::post('udemy/insert/token', 'ManagerController@addUdemyToken')->name("add.udemy.token");
    Route::get('udemy/add/token', 'ManagerController@udemyTokenDisplay')->name('udemy.token');
    Route::get('udemy/delete/token', 'ManagerController@deleteUdemyAccount')->name('delete.udemy.token');
    Route::get('udemy/delete/token/{udemy_id}', 'ManagerController@deleteUdemyAccount');





    Route::get('test2', function(){
        echo request()->getHost();
    });

});

Route::group(['prefix' => 'user', 'as' => 'user.', 'middleware' => ['auth', 'RoleUser']], function(){
    Route::get('dashboard',"LocalController@dashboard")->name('dashboard');
    Route::get('profile',"LocalController@profile")->name('profile');
    Route::get('extsession', "LocalController@extsession")->name('extsession');
    Route::post('extsession', "LocalController@extsession")->name('extsession2');
    
    Route::get('/api/v1/users/initial_authentication', "LocalController@extsession")->name('extsession3');
    Route::post('/api/v1/users/initial_authentication', "LocalController@extsession")->name('extsession4');
    
    
    Route::get('logout', function(){
        \Auth::logout();
        return redirect('login');
    })->name('logout');

    Route::get('jsweb', function(){
        return view('user.loading');
    })->name('js-web');
    Route::get('helium', function(){
        return view('user.helium-loading');
    })->name('helium-web');

    Route::get('helium/ext1', function(){
        return view('user.helium-loading');
    })->name('helium-web1');

    Route::get('helium/ext2', function(){
        return view('user.helium-loading');
    })->name('helium-web2');

    // Route::get('js-session', function(){
    //     return '[{"domain":".members.junglescout.com","expirationDate":1652303537,"hostOnly":false,"httpOnly":false,"name":"_vwo_uuid_v2","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"DF3C9AB590AD209113A72FAC422F3028C|45b46cf91558f79fbe5cc8771ceb2a75"},{"domain":".junglescout.com","expirationDate":1620682941,"hostOnly":false,"httpOnly":false,"name":"_hjAbsoluteSessionInProgress","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"0"},{"domain":"members.junglescout.com","expirationDate":1623265200,"hostOnly":true,"httpOnly":false,"name":"auth_token","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MjEyODU5NDAsImlhdCI6MTYyMDY4MTE0MCwiaXNzIjoianVuZ2xlc2NvdXRfYXBpIiwiYXVkIjoiY2xpZW50IiwiYXV0aF90b2tlbiI6IjcwYjVmZGNhY2ExMzFhYTRmZWE1ZjlmMTE2NGQwNDA5In0.zQLvYX8xXeQUlGmA67y5CZfDvqUUJcpxPXJKUkRTm3k"},{"domain":".junglescout.com","expirationDate":1620767542,"hostOnly":false,"httpOnly":false,"name":"_gid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"GA1.2.2095566102.1620681130"},{"domain":".junglescout.com","expirationDate":1622085137,"hostOnly":false,"httpOnly":false,"name":"_uetvid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"423667a0ae5c11eb81648f67839a268d"},{"domain":".junglescout.com","expirationDate":1628457139,"hostOnly":false,"httpOnly":false,"name":"_rdt_uuid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"1620681139188.11f9c4aa-a943-4e63-a111-71db0dce8c56"},{"domain":"members.junglescout.com","expirationDate":1620681259,"hostOnly":true,"httpOnly":false,"name":"_hjIncludedInSessionSample","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"1"},{"domain":".junglescout.com","expirationDate":1636233137,"hostOnly":false,"httpOnly":false,"name":"optimizelyEndUserId","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"oeu1620681137202r0.47678251720898857"},{"domain":".members.junglescout.com","expirationDate":1620681189,"hostOnly":false,"httpOnly":false,"name":"_dc_gtm_UA-52913301-4","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"1"},{"domain":".junglescout.com","expirationDate":1628457139,"hostOnly":false,"httpOnly":false,"name":"_gcl_au","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"1.1.1109134784.1620681139"},{"domain":".junglescout.com","expirationDate":1683753142,"hostOnly":false,"httpOnly":false,"name":"_ga","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"GA1.2.1799187749.1620299681"},{"domain":".members.junglescout.com","expirationDate":1683753141,"hostOnly":false,"httpOnly":false,"name":"_ga","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"GA1.3.1799187749.1620299681"},{"domain":".members.junglescout.com","expirationDate":1620682942,"hostOnly":false,"httpOnly":false,"name":"__stripe_sid","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"c03ca5c6-6624-43b7-a248-1e29e7fafdb060229f"},{"domain":".junglescout.com","expirationDate":1652217139,"hostOnly":false,"httpOnly":false,"name":"_hjid","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"750038ba-3981-4567-8a1c-be731b7d0449"},{"domain":".junglescout.com","expirationDate":1620681197,"hostOnly":false,"httpOnly":false,"name":"_gat_UA-52913301-4","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"1"},{"domain":".junglescout.com","expirationDate":1746911537,"hostOnly":false,"httpOnly":false,"name":"__ssid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"439b7b2546bec5922c59a57be05ebc1"},{"domain":"members.junglescout.com","hostOnly":true,"httpOnly":false,"name":"js-fullstory","path":"/","sameSite":null,"secure":false,"session":true,"storeId":null,"value":"false"},{"domain":".members.junglescout.com","expirationDate":1652217142,"hostOnly":false,"httpOnly":false,"name":"__stripe_mid","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"c5be1dd8-3596-464d-af1a-0653f322c0578e6a55"},{"domain":"members.junglescout.com","expirationDate":1620682040,"hostOnly":true,"httpOnly":false,"name":"_dd_s","path":"/","sameSite":"strict","secure":false,"session":false,"storeId":null,"value":"rum=0&expire=1620682040899"},{"domain":".junglescout.com","expirationDate":1628457142,"hostOnly":false,"httpOnly":false,"name":"_fbp","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"fb.1.1620681129829.778636296"},{"domain":".junglescout.com","expirationDate":1683753137,"hostOnly":false,"httpOnly":false,"name":"_ga_9C02FJRLCF","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"GS1.1.1620681123.1.1.1620681137.0"},{"domain":".members.junglescout.com","expirationDate":1620767541,"hostOnly":false,"httpOnly":false,"name":"_gid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"GA1.3.2095566102.1620681130"},{"domain":"members.junglescout.com","expirationDate":1620681259,"hostOnly":true,"httpOnly":false,"name":"_hjIncludedInPageviewSample","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"1"},{"domain":".junglescout.com","hostOnly":false,"httpOnly":false,"name":"_hjTLDTest","path":"/","sameSite":"lax","secure":false,"session":true,"storeId":null,"value":"1"},{"domain":"members.junglescout.com","expirationDate":1966194739,"hostOnly":true,"httpOnly":false,"name":"_omappvp","path":"/","sameSite":"lax","secure":true,"session":false,"storeId":null,"value":"GKDeMbO4fd1J1PFEYNhPVQsGENkqJUSa1lOJPdLdSjg7gZqKUfVoUfHzq3IAuvHcIRivJuSkEz7NyCNbkK5LnijYrPso7xvM"},{"domain":"members.junglescout.com","expirationDate":1620682339,"hostOnly":true,"httpOnly":false,"name":"_omappvs","path":"/","sameSite":"lax","secure":true,"session":false,"storeId":null,"value":"1620681139198"},{"domain":".members.junglescout.com","expirationDate":1652217139,"hostOnly":false,"httpOnly":false,"name":"_pin_unauth","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"dWlkPU9EWTNabVJrTUdRdFpqSmxPQzAwWWpCaUxUbG1NR1F0WVRreVltWmxNalF5TmpKaA"},{"domain":".junglescout.com","expirationDate":1620767537,"hostOnly":false,"httpOnly":false,"name":"_uetsid","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"61c72220b16c11eb8c48d1e10c2b3213"},{"domain":".junglescout.com","hostOnly":false,"httpOnly":false,"name":"_vis_opt_test_cookie","path":"/","sameSite":null,"secure":false,"session":true,"storeId":null,"value":"1"},{"domain":".junglescout.com","expirationDate":1652217146,"hostOnly":false,"httpOnly":false,"name":"ajs_anonymous_id","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"%22f1bb4f0e-e4b6-48fb-b142-bf8ec09ae6d9%22"},{"domain":".junglescout.com","expirationDate":1652217146,"hostOnly":false,"httpOnly":false,"name":"ajs_user_id","path":"/","sameSite":"lax","secure":false,"session":false,"storeId":null,"value":"655607"},{"domain":".junglescout.com","expirationDate":1623273142,"hostOnly":false,"httpOnly":false,"name":"membershipType","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"Web App Pro Monthly"},{"domain":".junglescout.com","expirationDate":1652217137,"hostOnly":false,"httpOnly":false,"name":"mp_c74f4284138a8d26cd2c3b5ebba43d0f_mixpanel","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"%7B%22distinct_id%22%3A%20%22179581f9c69470-02a00c625cc0df-52301645-1fa400-179581f9c6a5f3%22%2C%22%24device_id%22%3A%20%22179581f9c69470-02a00c625cc0df-52301645-1fa400-179581f9c6a5f3%22%2C%22%24initial_referrer%22%3A%20%22https%3A%2F%2Fmembers.junglescout.com%2F%22%2C%22%24initial_referring_domain%22%3A%20%22members.junglescout.com%22%7D"},{"domain":".junglescout.com","expirationDate":1623273142,"hostOnly":false,"httpOnly":false,"name":"userEmail","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"menigerroc03938@gmail.com"},{"domain":".junglescout.com","expirationDate":1623273142,"hostOnly":false,"httpOnly":false,"name":"userId","path":"/","sameSite":null,"secure":false,"session":false,"storeId":null,"value":"655607"}]';
    // })->name('js-session');

    Route::get('js-session', "ManagerController@webSessionReturn")->name('js-session');
    Route::get('helium-session', "ManagerController@heliumSessionReturn")->name('helium-session');
    Route::get('helium-ext1', "ManagerController@heliumSession1Return")->name('helium-session1');
    Route::get('helium-ext2', "ManagerController@heliumSession2Return")->name('helium-session2');


    Route::get('keepa', function(){
        return "<script>window.location.href = \"https://keepa.com\";</script>";
    })->name("keepa-tab");

    Route::get('udemy', function(){
        return "<script>window.location.href = \"https://udemy.com\";</script>";
    })->name("udemy-tab");
//    Route::get("keepa-session", function(){
//        return "519ctonnd8vuoerr9f13d4oltlf16p1elg0o7llk1p8em19gqkbp4sa2ddl0m6nb";
//    });
    Route::get('keepa-session', "ManagerController@keepaSessionReturn")->name('keepa-session');
    Route::get('keepa-session-1', "ManagerController@keepaSessionReturn_1")->name('keepa-session1');


    Route::get('udemy-session', "ManagerController@udemySessionReturn")->name('udemy-session');



});

Route::group(['prefix' => 'panel', 'as' => 'panel.', 'middleware' => 'auth'], function(){

});

