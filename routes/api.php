<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/v1/users/initial_authentication', "LocalController@extsession")->name('extsession3');
Route::post('/v1/users/initial_authentication', "LocalController@extsession")->name('extsession4');
    

Route::get("{any}", "ScoutController@getReceiver")->where('any', '.*');
Route::post("{any}", "ScoutController@postReceiver")->where('any', '.*');
