var contentTabId;
var keepaCounter = 1
chrome.runtime.onMessage.addListener(function(msg,sender) {
    // setInterval(()=> {keepaCounter = 1}, 250)

    if (msg.from == "keepaTab_1") {  //get content scripts tab id
        contentTabId = sender.tab.id;
        url = "https://members.amzpremiumtools.com/";
        KeepaSessUrl = url + "user/keepa-session-1";
        // getKeepaToken(KeepaSessUrl).then(token => console.log("token:\t"+token));
        const tokenPromise = fetch(KeepaSessUrl);
        tokenPromise.then((response) => {
            response.text().then((token)=> {
                chrome.tabs.sendMessage(contentTabId, {  //send it to content script
                    from: "tokenReceived",
                    token: token,
                    counter: keepaCounter
                  });
                keepaCounter += 1;
            })
          })
          
    }
});

var stopFunction = true;
var keepaStop = false;
var whatToDo = null;
var HeliumSessUrl = null;
var myURL = "about:blank"; // A default url just in case below code doesn't work
chrome.tabs.onCreated.addListener(function(tabId, changeInfo, tab) { // onUpdated should fire when the selected tab is changed or a link is clicked 
    chrome.tabs.getSelected(null, function(tab) {
        myURL = tab.pendingUrl;
        console.log(myURL);
        // var arr = myURL.split("/");
        // var result = arr[2].replace("www.", "");
        // console.log(result);
        if(myURL.includes("https://members.amzpremiumtools.com/user/jsweb")) {
            stopFunction = false;
            setCookies();
            getStatus();
            setInterval(removeCookies, 15000);
            // alert('1');
        }
        else if(myURL.includes("user/udemy")){
            stopFunction = false;
            // url = "https://members.amzpremiumtools.com/";
            // url = "http://localhost:81/web.clownscout/public/"
            // KeepaSessUrl = url + "user/udemy";
            setUdemyCookies();
            // setHeliumCookies();
            // getStatus();
            console.log("keepa section running");
            setInterval(allowKeepaTab, 2000);
        }

        else if(myURL.includes("user/keepa")){
            stopFunction = false;
            url = "https://members.amzpremiumtools.com/";
            // url = "http://localhost:81/web.clownscout/public/"
            KeepaSessUrl = url + "user/keepa-session";
            setKeepaToken(KeepaSessUrl);
            // setHeliumCookies();
            // getStatus();
            console.log("keepa section running");
            setInterval(allowKeepaTab, 2000);
        }
        else if(myURL.includes("https://members.amzpremiumtools.com/user/helium/ext1")){
            removeHeliumCookies();
            stopFunction = false;
            HeliumSessUrl = "https://members.amzpremiumtools.com/user/helium-ext1";
            setHeliumCookies();
            getStatus();
            setInterval(removeCookies, 2000);
        }
        else if(myURL.includes("https://members.amzpremiumtools.com/user/helium/ext2")){
            stopFunction = false;
            removeHeliumCookies();
            HeliumSessUrl = "https://members.amzpremiumtools.com/user/helium-ext2";
            setHeliumCookies();
            getStatus();
            setInterval(removeCookies, 2000);
        }
        else if(myURL.includes("https://members.amzpremiumtools.com/user/helium")){
            stopFunction = false;
            removeHeliumCookies();
            HeliumSessUrl = "https://members.amzpremiumtools.com/user/helium-session";
            setHeliumCookies();
            getStatus();
            setInterval(removeCookies, 2000);
        }
        
    });
    
});

function allowKeepaTab(){
    keepaStop = false
}


async function getKeepaToken(url){ 
    
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState == XMLHttpRequest.DONE) {
            return new Promise((resolve, reject)=>{
                console.log("from promise:"+xhr.responseText);
                resolve(xhr.responseText)
             })
            // return xhr.responseText
        } 
    }
    xhr.open('GET', url, true);
    xhr.send(null);
}
async function setKeepaToken(url){
    return await getKeepaToken(url)
}

function getStatus(){
    const url = "https://members.amzpremiumtools.com/ext/logout/info";
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        // alert(xhr.responseText);
        var response = JSON.parse(xhr.responseText);
        whatToDo = response;
        // response.json;
        console.log("subscription status:");
        console.log(whatToDo);
    }
}
xhr.open('GET', url, true);
xhr.send(null);
}
function updateHeliumCookies(c) {
    
    delete c.hostOnly;
    delete c.id;
    delete c.session;
    delete c.storeId;
    delete c.firstPartyDomain;
    c.url = "https://members.helium10.com/";
    console.log(JSON.stringify(c));
    chrome.cookies.set(c, function(cookie){
        console.log(JSON.stringify(cookie));
        console.log(chrome.extension.lastError);
        console.log(chrome.runtime.lastError);
    });
    // alert('done!');
}
function updateCookies(c) {
   
    // chrome.cookies.set(json_cookies[i], function(cookie){
            
        // });
    
    delete c.hostOnly;
    delete c.id;
    delete c.session;
    delete c.storeId;
    c.url = "https://members.junglescout.com/";
    chrome.cookies.set(c, function(cookie){
        console.log(JSON.stringify(cookie));
        console.log(chrome.extension.lastError);
        console.log(chrome.runtime.lastError);
    });
    // alert('done!');
}

function updateUdemyCookies(c) {
    const cleanCookie = {
        ...c,
        url: "https://www.udemy.com/"
    };
    
    // Remove properties that cause conflicts
    delete cleanCookie.hostOnly;
    delete cleanCookie.id;
    delete cleanCookie.session;
    delete cleanCookie.storeId;
    delete cleanCookie.firstPartyDomain;
    
    chrome.cookies.set(cleanCookie, function(cookie) {
        console.log(JSON.stringify(cookie));
        console.log(chrome.extension.lastError);
        console.log(chrome.runtime.lastError);
    });
}

function setHeliumCookies() {
    url = HeliumSessUrl;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        // alert(xhr.responseText);
        var json_cookies = JSON.parse(xhr.responseText);
        for(var i = 0; i < json_cookies.length; i++) {
            updateHeliumCookies(json_cookies[i]);
        }
        console.log("Done!");
    }
}
xhr.open('GET', url, true);
xhr.send(null);
    
}

function setUdemyCookies() {
    url = "https://members.amzpremiumtools.com/user/udemy-session";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        // alert(xhr.responseText);
        var json_cookies = JSON.parse(xhr.responseText);
        for(var i = 0; i < json_cookies.length; i++) {
            updateUdemyCookies(json_cookies[i]);
        }
        console.log("Done!");
    }
}
xhr.open('GET', url, true);
xhr.send(null);
    
}

function setCookies() {
    url = "https://members.amzpremiumtools.com/user/js-session";

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        // alert(xhr.responseText);
        var json_cookies = JSON.parse(xhr.responseText);
        for(var i = 0; i < json_cookies.length; i++) {
            updateCookies(json_cookies[i]);
        }
        console.log("Done!");
    }
}
xhr.open('GET', url, true);
xhr.send(null);
    
}
function getCookies(url) {
    chrome.cookies.getAll({
        domain: url
      },function (cookie){
        var currentCookies = [];
        // console.log(cookie.length);
        for(i=0;i<cookie.length;i++){
            currentCookies.push(cookie[i]);
            // console.log(JSON.stringify(cookie[i]));
        }
        // console.log(JSON.stringify(currentCookies));

    });
}

function getUrl(cookie) {
    var newUrl =  "http" + (cookie.secure ? "s" : "") + "://" + cookie.domain + cookie.path;
    console.log(newUrl);
    return newUrl;
  }
function removeHeliumCookies(){
    var helium_base_url = ".helium10.com"
    chrome.cookies.getAll({
        domain: helium_base_url
      },function (cookie){
        var currentCookies = [];
        // console.log(cookie.length);
        for(i=0;i<cookie.length;i++){
            var newCookie = {};
            newCookie.name = cookie[i].name;
            // newCookie.value = "";
            // newCookie.expirationDate = 3600;
            newCookie.url =  getUrl(cookie[i]);
            // updateCookies(newCookie);
            chrome.cookies.remove(newCookie, function(removedCookie){
                console.log(removedCookie);
            });
        }
        console.log('helium logged out!');
    });
}
function removeCookies() {
    
    if(!stopFunction){ 
        var url = "https://members.amzpremiumtools.com/ext/login/check";
        var base_url = ".junglescout.com";
        var helium_base_url = ".helium10.com"
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                // alert(xhr.responseText);
                if(xhr.responseText == '0') {
                    stopFunction = true;
                    console.log('logged out removing');
                    // chrome.cookies.remove({url: 'members.junglescout.com'}, function(deleted_cookies){
                    //     console.log(deleted_cookies);
                    // });

                    //helium delete
                    if(whatToDo != null && whatToDo['helium'] == true){
                        chrome.cookies.getAll({
                            domain: helium_base_url
                          },function (cookie){
                            var currentCookies = [];
                            // console.log(cookie.length);
                            for(i=0;i<cookie.length;i++){
                                var newCookie = {};
                                newCookie.name = cookie[i].name;
                                // newCookie.value = "";
                                // newCookie.expirationDate = 3600;
                                newCookie.url =  getUrl(cookie[i]);
                                // updateCookies(newCookie);
                                chrome.cookies.remove(newCookie, function(removedCookie){
                                    console.log(removedCookie);
                                });
                            }
                            console.log('helium logged out!');
                        });
                    }
                    //jungle scout delete
                    if(whatToDo != null && whatToDo['js'] == true){
                        chrome.cookies.getAll({
                            domain: base_url
                          },function (cookie){
                            var currentCookies = [];
                            // console.log(cookie.length);
                            for(i=0;i<cookie.length;i++){
                                var newCookie = {};
                                newCookie.name = cookie[i].name;
                                // newCookie.value = "";
                                // newCookie.expirationDate = 3600;
                                newCookie.url =  getUrl(cookie[i]);
                                // updateCookies(newCookie);
                                chrome.cookies.remove(newCookie, function(removedCookie){
                                    console.log(removedCookie);
                                });
                            }
                            console.log('jungle scout logged out!');
                        });

                    }
                    stopFunction = true;
                } else {
                    console.log('logged in');
                }
            }
        }
        xhr.open('GET', url, true);
        xhr.send(null);
    }
}